package com.carvalab.mountandlaser.collision;

import java.util.ArrayList;
import java.util.Collection;

import com.carvalab.mountandlaser.core.GameObject;

public class GridBroadphase {

	private int width, height;
	private int division;
	private ArrayList<GameObject> grid[][];
	private ArrayList<GameObject> master;

	public GridBroadphase(int w, int h, int d) {
		width = w;
		height = h;
		division = d;
		master = new ArrayList<>();
	}

	@SuppressWarnings("unchecked")
	private void startData() {
		grid = new ArrayList[division][division];
	}
	
	public ArrayList<GameObject> getMaster() {
		return master;
	}

	private void makeGrid() {
		for (int r = 0; r < division; r++) {
			for (int c = 0; c < division; c++) {
				if (grid[c][r] == null)
					grid[c][r] = new ArrayList<>();
				else
					grid[c][r].clear();
			}
		}
	}

	public void addObjectToMaster(GameObject obj) {
		if (!master.contains(obj))
			master.add(obj);
	}

	public void addObject(GameObject o) {
		// Check if the object is inside the grid
		if (o.getPos().x < 0 || o.getPos().x > width)
			return;
		if (o.getPos().y < 0 || o.getPos().y > height)
			return;

		// Get the column and row
		int column = (int) ((o.getPos().x / width) * division);
		int row = (int) ((o.getPos().y / height) * division);

		// Add to the cell
		if (column < 0)
			column = 0;
		if (row < 0)
			row = 0;
		if (row > division - 1)
			row = division - 1;
		if (column > division - 1)
			column = division - 1;
		grid[column][row].add(o);
		// System.out.println("Object "+o.getID()+" added at: "+column+" "+row);

		// Add overlaps
		int maxColumn = (int) (((o.getPos().x + o.getWidth()) / width) * division);
		int maxRow = (int) (((o.getPos().y + o.getHeight()) / height) * division);

		if (maxColumn >= division)
			maxColumn = division - 1;
		if (maxRow >= division)
			maxRow = division - 1;

		// Check what overlaps
		// Add to the right
		if (maxColumn > column) {
			grid[maxColumn][row].add(o);
		}
		// Add to the bottom
		if (maxRow > row) {
			grid[column][maxRow].add(o);
		}
		// Add to bottm right
		if (maxRow > row && maxColumn > column) {
			grid[maxColumn][maxRow].add(o);
		}
	}

	public void buildAndApply(Collection<? extends GameObject> objs, INarrowPhase narrow,
			ICollisionSolver solver) {
		startData();
		if (division == 0)
			throw new RuntimeException(
					"Can't run collisions with no rows or columns.");

		// Build the grid
		makeGrid();

		// Add each object
		//Also apply narrow and solver with master
		for (GameObject u : objs) {
			addObject(u);
			
			//Master list
			for(GameObject o : master) {
				if (narrow.collide(u, o)) {
					u.collided(o);
					o.collided(u);
					solver.solve(u, o);
				}
			}
		}

		// Iterate the grid and apply narrow phase
		for (int r = 0; r < division; r++) {
			for (int c = 0; c < division; c++) {
				for (int u = 0; u < grid[c][r].size() - 1; u++) {// Only need to
																	// go to the
																	// n-1
					for (int u2 = u + 1; u2 < grid[c][r].size(); u2++) {// Starts
																		// from
																		// n+1
						GameObject a = grid[c][r].get(u);
						GameObject b = grid[c][r].get(u2);
						if (narrow.collide(a, b)) {
							a.collided(b);
							b.collided(a);
							solver.solve(a, b);
						}
						// System.out.println("testing "+grid[c][r].get(u).getID()+" against "+grid[c][r].get(u2).getID());
					}
				}
			}
		}
	}

}
