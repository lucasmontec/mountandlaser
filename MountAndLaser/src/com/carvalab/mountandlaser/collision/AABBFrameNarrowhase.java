package com.carvalab.mountandlaser.collision;

import java.awt.Rectangle;

import com.carvalab.mountandlaser.core.GameObject;
import com.carvalab.mountandlaser.unit.Unit;

public class AABBFrameNarrowhase implements INarrowPhase {

	@Override
	public boolean collide(GameObject a, GameObject b) {
		if (a instanceof Unit && b instanceof Unit) {
			Rectangle AABB_a = new Rectangle((int) ((Unit) a).X(),
					(int) ((Unit) a).Y(), ((Unit) a).getFrameWidth(),
					((Unit) a).getFrameHeight());
			Rectangle AABB_b = new Rectangle((int) ((Unit) b).X(),
					(int) ((Unit) b).Y(), ((Unit) b).getFrameWidth(),
					((Unit) b).getFrameHeight());
			return AABB_a.intersects(AABB_b);
		} else
			return false;
	}
}
