package com.carvalab.mountandlaser.collision;

import com.carvalab.mountandlaser.core.GameObject;

public interface INarrowPhase {

	public boolean collide(GameObject a, GameObject b);
	
}
