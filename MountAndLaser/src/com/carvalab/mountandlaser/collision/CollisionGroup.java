package com.carvalab.mountandlaser.collision;

public enum CollisionGroup {

	PROJECTILE, WORLD, PLAYER;
	
	public static boolean shouldCollide(CollisionGroup a, CollisionGroup b) {
		//Projectiles don't collide with projectiles
		if(a.equals(CollisionGroup.PROJECTILE) && a.equals(b))
			return false;
		
		if(a.equals(CollisionGroup.WORLD) && a.equals(b))
			return false;
		
		return true;
	}
	
}
