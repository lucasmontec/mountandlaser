package com.carvalab.mountandlaser.collision;

import com.carvalab.mountandlaser.core.GameObject;

public class ShapeNarrowphase implements INarrowPhase {

	@Override
	public boolean collide(GameObject a, GameObject b) {
		if (CollisionGroup.shouldCollide(a.getBody().getCollisionGroup(), b
				.getBody().getCollisionGroup()))
			return a.getBody().intersects(b.getBody());
		return false;
	}

}
