package com.carvalab.mountandlaser.collision;

import com.carvalab.mountandlaser.core.GameObject;

public interface ICollisionSolver {

	public void solve(GameObject a, GameObject b);
	
}
