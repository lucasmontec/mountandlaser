package com.carvalab.mountandlaser.collision;

import java.util.Random;

import com.carvalab.mountandlaser.core.GameObject;
import com.carvalab.mountandlaser.framework.geom.Circunference;
import com.carvalab.mountandlaser.framework.geom.Intersections;
import com.carvalab.mountandlaser.framework.geom.Rectangle;
import com.carvalab.mountandlaser.unit.Unit;

public class ShapeSimpleBounceSolver implements ICollisionSolver {

	private final Random rand = new Random();

	@Override
	public void solve(GameObject a, GameObject b) {
		// Remove collision
		// Circ to circ
		if (a.getBody() instanceof Circunference
				&& b.getBody() instanceof Circunference) {
			if (a.isMovable())
				a.getPos().sub(
						Intersections.minimumTranslationCircToCirc(
								(Circunference) a.getBody(),
								(Circunference) b.getBody()));
			else
				b.getPos().sub(
						Intersections.minimumTranslationCircToCirc(
								(Circunference) b.getBody(),
								(Circunference) a.getBody()));
		}
		// Rect and circ
		if (a.getBody() instanceof Circunference
				&& b.getBody() instanceof Rectangle) {
			if (a.isMovable())
				a.getPos().sub(
						Intersections.minimumTranslationCircToRect(
								(Circunference) a.getBody(),
								(Rectangle) b.getBody()));
		}
		if (a.getBody() instanceof Rectangle
				&& b.getBody() instanceof Circunference) {
			if (b.isMovable())
				b.getPos().sub(
						Intersections.minimumTranslationCircToRect(
								(Circunference) b.getBody(),
								(Rectangle) a.getBody()));
		}
		// Rect to rect
		if (a.getBody() instanceof Rectangle
				&& b.getBody() instanceof Rectangle) {
			if (a.isMovable())
				a.getPos().add(
						Intersections.minimumTranslationRectToRect(
								(Rectangle) a.getBody(),
								(Rectangle) b.getBody()));
			else
				b.getPos().add(
						Intersections.minimumTranslationRectToRect(
								(Rectangle) b.getBody(),
								(Rectangle) a.getBody()));
		}

		// Inverse and mult speed
		if (a instanceof Unit)
			((Unit) a).getVelocity().mulX(rand.nextFloat())
					.mulY(rand.nextFloat()).mul(-0.7f);
		if (b instanceof Unit)
			((Unit) b).getVelocity().mulX(rand.nextFloat())
					.mulY(rand.nextFloat()).mul(-0.7f);
	}

}
