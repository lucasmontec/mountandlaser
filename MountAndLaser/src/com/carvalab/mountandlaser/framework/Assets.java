package com.carvalab.mountandlaser.framework;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.carvalab.mountandlaser.core.Animation;

public class Assets {

	public static final String assets = "assets";

	public static Animation bullet;
	public static Animation enemy_idle;
	public static Animation enemy_dead;

	public static Animation getEnemyIdle() {
		if(enemy_idle == null)
			enemy_idle = animationState("demon","idle",0.08f).setLooping(true).setPingPong(true);
		return enemy_idle;
	}
	
	public static void loadGameAssets() {
		Thread t = new Thread(() -> {
			bullet = animation("bullet",0.2f).setLooping(true);
		});
		t.start();
	}
	
	public static void unloadGameAssets() {
		bullet = null;
	}

	public static File imagesDirectory() {
		return new File(assets + File.separator + "images");
	}

	public static Animation animationState(String name, String state,
			float frameDuration) {
		return animation(name + "_" + state, frameDuration);
	}

	public static Animation animation(String name, float frameDuration) {
		Animation ret = new Animation(frameDuration);

		File imagesDir = imagesDirectory();

		int index = 0;
		// Read first image
		File frameFile = new File(imagesDir.getPath() + File.separator + name
				+ "_" + (index++) + ".png");
		System.out.println("Creating anim: " + frameFile.getAbsolutePath());

		// Read until there is no more image to load
		while (frameFile.exists()) {
			BufferedImage frameImage = null;
			try {
				frameImage = ImageIO.read(frameFile);
			} catch (IOException e) {
				System.err.println("Couldn't read image: "
						+ frameFile.getName());
			}

			if (frameImage != null) {
				System.out.println("Added image to anim: " + frameFile);
				ret.addFrame(frameImage);
			} else
				System.err.println("Null frame: " + frameFile);

			// Read subsequent images
			frameFile = new File(imagesDir.getPath() + File.separator + name
					+ "_" + (index++) + ".png");
		}
		
		return ret;
	}

}
