package com.carvalab.mountandlaser.framework.camera;

import java.awt.Graphics2D;

import com.carvalab.mountandlaser.framework.geom.Point;
import com.carvalab.mountandlaser.framework.geom.Rectangle;
import com.carvalab.mountandlaser.unit.Unit;

public class Camera {

	protected Point focus;
	protected Rectangle bounds;
	
	public Camera(int width, int height) {
		focus = new Point();
		bounds = new Rectangle(width, height);
		bounds.setCenter(focus);
	}
	
	public void film(Graphics2D g) {
		bounds.setCenter(focus);
		g.translate(-focus.x+bounds.width/2, -focus.y+bounds.height/2);
	}
	
	public void restore(Graphics2D g) {
		g.translate(focus.x-bounds.width/2, focus.y-bounds.height/2);
	}
	
	public boolean inView(Unit u) {
		return u.getBody().intersects(bounds);
	}
	
	public Point topLeft() {
		return bounds.topLeft;
	}
	
	public Point getFocus() {
		return focus;
	}

	public void setFocus(Point focus) {
		bounds.setCenter(focus);
		this.focus = focus;
	}

	public Rectangle getBounds() {
		return bounds;
	}
	
}
