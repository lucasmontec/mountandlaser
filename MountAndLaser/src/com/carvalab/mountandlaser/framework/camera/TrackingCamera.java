package com.carvalab.mountandlaser.framework.camera;

import java.awt.Graphics2D;

import com.carvalab.mountandlaser.unit.Unit;

public class TrackingCamera extends Camera {

	private Unit target;
	private float trackSpeed;
	private final float deadzone = 8f;

	public TrackingCamera(int width, int height, Unit target) {
		this(width, height, target, 0.04f);
	}

	public TrackingCamera(int width, int height, Unit target, float trackspeed) {
		super(width, height);
		this.target = target;
		this.trackSpeed = trackspeed;
		setFocus(target.getBody().center().copy());
		bounds.setCenter(focus);
	}

	@Override
	public void film(Graphics2D g) {
		if(focus.distance2(target.getBody().center())>deadzone)
		focus.add(target.getBody().center().copy().sub(focus).asVectorNormalize().mul(trackSpeed));
		//focus.add(0.01f, 0.01f);
		super.film(g);
	}
	
}
