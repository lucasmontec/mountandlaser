package com.carvalab.mountandlaser.framework;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferStrategy;
import java.util.HashMap;

import javax.swing.event.MouseInputListener;

public class ScreenRenderer extends Canvas implements MouseMotionListener,
		MouseInputListener, KeyListener, Runnable {
	private static final long serialVersionUID = -2847762841919544343L;

	private Thread thread;
	private boolean running;
	private long lastTime = 0;

	private HashMap<String, Screen> screens;
	public Screen current;
	private BufferStrategy bufferStrategy;

	public ScreenRenderer() {
		running = false;
		setIgnoreRepaint(true);
		screens = new HashMap<>();
	}

	public void start() {
		running = true;
		thread = new Thread(this);
		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);
		thread.start();
	}

	/**
	 * The game loop
	 */
	public void run() {
		// Prepare the buffers
		bufferStrategy = getBufferStrategy();
		if (bufferStrategy == null) {
			createBufferStrategy(2);
			bufferStrategy = getBufferStrategy();
		}

		// Load first time
		lastTime = System.currentTimeMillis();

		// Run the game
		while (running) {
			// Update
			if (current != null) {
				float delta = (System.currentTimeMillis() - lastTime )/1000f;
				current.update(delta);
				current.delta = delta;
			}

			// Time control
			lastTime = System.currentTimeMillis();

			// Render
			if (!bufferStrategy.contentsLost()) {
				Graphics2D g = (Graphics2D) bufferStrategy.getDrawGraphics();

				// Background and antialiasing
				g.setColor(Color.black);
				g.fillRect(0, 0, getWidth(), getHeight());

				// Draw current screen
				if (current != null && g instanceof Graphics2D) {
					g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
							RenderingHints.VALUE_ANTIALIAS_ON);
					current.render(g);
				}

				// Flip and flush
				bufferStrategy.show();
			}
		}
	}

	/**
	 * @param name
	 *            The screen to change to
	 */
	public void changeScreen(String name) {
		if (screens.containsKey(name)) {
			if (current != null)
				current.leave();
			current = screens.get(name);
			current.enter();
		} else
			throw new RuntimeException(
					"Screen with this name is not registered.");
	}

	/**
	 * Adds a screen to this manager
	 * 
	 * @param name
	 *            The screen key
	 * @param screen
	 *            The screen object
	 */
	public void addScreen(String name, Screen screen) {
		screen.WIDTH = getWidth();
		screen.HEIGHT = getHeight();
		screens.put(name, screen);
		changeScreen(name);
	}

	@Override
	public void resize(int w, int h) {
		for (Screen s : screens.values()) {
			s.WIDTH = w;
			s.HEIGHT = h;
		}
	}

	@Override
	public void mouseClicked(MouseEvent evt) {
	}

	@Override
	public void mouseEntered(MouseEvent evt) {
	}

	@Override
	public void mouseExited(MouseEvent evt) {
		if (current != null) {
			current.MOUSE_DRAGGING = false;
		}
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		if (current != null) {
			current.MOUSE_LEFT = (evt.getButton() == MouseEvent.BUTTON1);
			current.MOUSE_MIDDLE = (evt.getButton() == MouseEvent.BUTTON2);
			current.MOUSE_RIGHT = (evt.getButton() == MouseEvent.BUTTON3);
			current.mousePressed();
		}
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (current != null) {
			current.MOUSE_LEFT = !(evt.getButton() == MouseEvent.BUTTON1);
			current.MOUSE_MIDDLE = !(evt.getButton() == MouseEvent.BUTTON2);
			current.MOUSE_RIGHT = !(evt.getButton() == MouseEvent.BUTTON3);
			current.MOUSE_DRAGGING = false;
			current.mouseReleased();
		}
	}

	@Override
	public void mouseDragged(MouseEvent evt) {
		if (current != null) {
			current.MOUSE.set(evt.getPoint());
			current.MOUSE_DRAGGING = true;
			current.mouseDragging();
		}
	}

	@Override
	public void mouseMoved(MouseEvent evt) {
		if (current != null) {
			current.MOUSE.set(evt.getPoint());
		}
	}

	public void stop() {
		running = false;
		bufferStrategy.dispose();
	}

	@Override
	public void keyPressed(KeyEvent evt) {
		if (current != null)
			current.keyPressed(evt);
		current.keys[evt.getKeyCode()] = true;
	}

	@Override
	public void keyReleased(KeyEvent evt) {
		if (current != null)
			current.keyReleased(evt);
		current.keys[evt.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}
}
