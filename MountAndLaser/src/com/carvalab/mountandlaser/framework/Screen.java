package com.carvalab.mountandlaser.framework;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import com.carvalab.mountandlaser.framework.geom.Point;

public abstract class Screen {

	// Variables
	protected int WIDTH;
	protected int HEIGHT;

	// Input
	protected boolean MOUSE_LEFT = false;
	protected boolean MOUSE_MIDDLE = false;
	protected boolean MOUSE_RIGHT = false;

	protected Point MOUSE = new Point();

	protected boolean MOUSE_DRAGGING = false;
	
	protected float delta;
	
	protected boolean keys[] = new boolean[300];//256 would do I think

	// Methods

	public abstract void render(Graphics2D g);

	public abstract void update(float delta);

	public void mousePressed() {}

	public void mouseReleased() {}

	public void mouseDragging() {}

	public abstract void enter();

	public abstract void leave();
	
	public void keyPressed(KeyEvent ke) {}

	public void keyReleased(KeyEvent ke) {}
	
	protected boolean isKeyPressed(int keycode) {
		return keys[keycode];
	}
}
