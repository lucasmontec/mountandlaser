package com.carvalab.mountandlaser.framework.ai;

import com.carvalab.mountandlaser.framework.geom.Point;

public abstract class Heuristic {

	public abstract float eval(Point p);

	/**
	 * if eval(a) is better then eval(b) return 0 else return 1.
	 */
	public int compare(Point a, Point b) {
		// The distance a to target is smaller then b's?
		// if so, return 0 (a is best), else return 1 (b is best)
		return (eval(a) < eval(b) ? 0 : 1);
	}
}
