package com.carvalab.mountandlaser.framework.ai;

import com.carvalab.mountandlaser.framework.geom.Point;

public class ProximityHeuristic extends Heuristic {

	private Point target;
	private Point[][] graph;

	public ProximityHeuristic(Point tgt, Point[][] graph) {
		target = tgt;
		this.graph = graph;
	}

	private Point getInGraph(Point p) {
		return graph[(int) p.x][(int) p.y];
	}

	/*@Override
	public int compare(Point a, Point b) {
		System.out
				.println("Distance a: " + eval(a) + " Distance b: " + eval(b));
		System.out.println("Choosen: " + (super.compare(a, b) == 0 ? "a" : "b"));
		return super.compare(a, b);
	}*/

	@Override
	public float eval(Point p) {
		return getInGraph(p).distance(target);
	}

}
