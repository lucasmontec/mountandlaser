package com.carvalab.mountandlaser.framework.ai;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Stack;

import com.carvalab.mountandlaser.core.GameObject;
import com.carvalab.mountandlaser.framework.Map;
import com.carvalab.mountandlaser.framework.geom.Point;

public class Pathfinder {

	Point[][] graph;
	boolean[][] dynamicBlockedNodes;
	boolean[][] staticBlockedNodes;
	private int division, width, height;
	final Point[] validMovements = {
			// Cross
			new Point(1, 0), new Point(0, 1), new Point(-1, 0),
			new Point(0, -1),
			// Borders
			new Point(1, 1), new Point(-1, -1), new Point(-1, 1),
			new Point(1, -1) };

	public Pathfinder(Map m, int division) {
		graph = new Point[division][division];
		dynamicBlockedNodes = new boolean[division][division];
		staticBlockedNodes = new boolean[division][division];

		float xspace = m.getWidth() / division;
		float yspace = m.getHeight() / division;

		for (int x = 0; x < division; x++)
			for (int y = 0; y < division; y++) {
				graph[x][y] = new Point(xspace * x, yspace * y);
				dynamicBlockedNodes[x][y] = false;
				staticBlockedNodes[x][y] = false;
			}

		this.division = division;
		width = m.getWidth();
		height = m.getHeight();
		processMap(m);
	}

	public void renderDebug(Graphics2D g, ArrayList<Point> path,
			Point startpos, Point endpos) {
		g.setColor(Color.red);
		for (int x = 0; x < division; x++)
			for (int y = 0; y < division; y++)
				graph[x][y].renderDebug(g);

		g.setColor(Color.green);
		if (path != null)
			for (Point p : path)
				p.renderDebug2(g, 4);

		g.setColor(Color.BLUE);
		if (startpos != null)
			getNearestNode(startpos).renderDebug2(g, 6);

		g.setColor(Color.RED);
		if (endpos != null)
			getNearestNode(endpos).renderDebug2(g, 6);
	}

	public Point getNearestNodeIndex(Point p) {
		return getNearestNodeIndex(p.x, p.y);
	}

	public Point getNearestNodeIndex(float x, float y) {
		int idx = (int) ((x / width) * division) + 1;
		int idy = (int) ((y / height) * division) + 1;

		if (idx < 0)
			idx = 0;
		if (idx > graph.length - 1)
			idx = graph.length - 1;

		if (idy < 0)
			idy = 0;
		if (idy > graph[0].length - 1)
			idy = graph[0].length - 1;

		return new Point(idx, idy);
	}

	public Point getNearestNode(float x, float y) {
		int idx = (int) ((x / width) * division) + 1;
		int idy = (int) ((y / height) * division) + 1;

		if (idx < 0)
			idx = 0;
		if (idx > graph.length)
			idx = graph.length;

		if (idy < 0)
			idy = 0;
		if (idy > graph[0].length)
			idy = graph[0].length;

		return graph[idx][idy];
	}

	public Point getNearestNode(Point p) {
		return getNearestNode(p.x, p.y);
	}

	/**
	 * Static obstacle processing. Process the map adding all barriers as
	 * blocked nodes
	 * 
	 * @param map
	 *            The map beeing processed
	 */
	private void processMap(Map m) {
		ArrayList<? extends GameObject> mapobjs = m.getWalls();
		for (GameObject obj : mapobjs)
			for (int x = 0; x < division; x++)
				for (int y = 0; y < division; y++)
					if (obj.getBody().contains(graph[x][y]))
						staticBlockedNodes[x][y] = true;

	}

	private void resetDynamicBlockingNodes() {
		for (int x = 0; x < division; x++)
			for (int y = 0; y < division; y++)
				dynamicBlockedNodes[x][y] = false;
	}

	public void updateDynamicBlockingNodes(
			Collection<? extends GameObject> objects) {
		resetDynamicBlockingNodes();
		for (GameObject obj : objects)
			if (obj.isDynamicObstacle())
				for (int x = 0; x < division; x++)
					for (int y = 0; y < division; y++)
						if (obj.getBody().contains(graph[x][y]))
							dynamicBlockedNodes[x][y] = true;
	}

	public boolean isNodeBlocked(Point node) {
		return dynamicBlockedNodes[(int) node.x][(int) node.y]
				|| staticBlockedNodes[(int) node.x][(int) node.y];
	}

	/**
	 * 
	 * @param heuristic
	 * @param start
	 *            The desired start position
	 * @param end
	 *            The desired end position
	 * @return
	 */
	public ArrayList<Point> getHeuristicDFSPath(Heuristic heuristic,
			Point startpos, Point endpos) {
		// The final path
		ArrayList<Point> path = null;

		// The DFS Stack
		DFS.startDFS(division, division);
		Stack<Point> dfsStack = new Stack<>();

		Point start = getNearestNodeIndex(startpos);
		// System.out.println("start " + startpos + " startnode " + start);
		Point end = getNearestNodeIndex(endpos);
		// System.out.println("end " + endpos + " endnode " + end);

		// Add and visit root
		dfsStack.push(start);
		DFS.visit((int) start.x, (int) start.y);

		boolean found = false;

		// Navigate the graph
		while (!dfsStack.isEmpty() && !found) {
			Point current = dfsStack.peek();
			Point bestChild = null;

			if ((bestChild = DFS.getBestUnvisited(heuristic, (int) current.x,
					(int) current.y, this)) != null) {
				// Found
				// System.out.println(bestChild + ":" + end);
				if (bestChild.x == end.x && bestChild.y == end.y) {
					// System.out.println("end");
					found = true;
				}
				// Visit
				DFS.visit((int) bestChild.x, (int) bestChild.y);
				dfsStack.push(bestChild);
			} else {
				// System.out.println("deadend");
				// Dead end
				dfsStack.pop();
			}
		}

		// Get the path
		if (found && !dfsStack.isEmpty()) {
			path = new ArrayList<>();
			while (!dfsStack.isEmpty()) {
				Point p = dfsStack.pop();
				path.add(graph[(int) p.x][(int) p.y]);
			}
			Collections.reverse(path);
			// System.out.println(path);
		}

		return path;
	}

	public Point[][] getGraph() {
		return graph;
	}
}
