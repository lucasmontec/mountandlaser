package com.carvalab.mountandlaser.framework.ai;

import com.carvalab.mountandlaser.framework.geom.Point;

public class DirectProximityHeuristic extends Heuristic {

	private Point target;
	
	/*public int compare(Point a, Point b) {
		System.out
				.println("Distance a: " + eval(a) + " Distance b: " + eval(b));
		System.out.println("Choosen: " + (super.compare(a, b) == 0 ? "a" : "b"));
		return super.compare(a, b);
	}*/

	public void update(Pathfinder pathfinder, Point tgt) {
		target = pathfinder.getNearestNodeIndex(tgt);
	}
	
	@Override
	public float eval(Point p) {
		return p.distance(target);
	}

}
