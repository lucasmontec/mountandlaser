package com.carvalab.mountandlaser.framework.ai;

import java.util.ArrayList;

import com.carvalab.mountandlaser.framework.geom.Point;

public class DFS {

	private static boolean[][] visitedMap;

	private static void resetMap() {
		for (int i = 0; i < visitedMap.length; i++)
			for (int j = 0; j < visitedMap[0].length; j++)
				visitedMap[i][j] = false;
	}

	public static void startDFS(int xV, int yV) {
		if (visitedMap == null || visitedMap.length != xV
				|| visitedMap[0].length != yV)
			visitedMap = new boolean[xV][yV];
		resetMap();
	}

	public static boolean isVisited(int xV, int yV) {
		return visitedMap[xV][yV];
	}

	public static void visit(int xV, int yV) {
		visitedMap[xV][yV] = true;
	}

	public static ArrayList<Point> getUnvisitedList(int xV, int yV,
			Point[][] graph, Point[] validMovements) {
		ArrayList<Point> points = new ArrayList<>();
		Point source = new Point(xV, yV);

		// Use all movements
		for (Point movement : validMovements) {
			// Move source to see if it is inside the graph boundaries
			Point sourceMoved = source.copy().add(movement);
			if (sourceMoved.x > 0 && sourceMoved.x < graph.length)
				if (sourceMoved.y > 0 && sourceMoved.y < graph[0].length)
					points.add(sourceMoved);
		}

		return points;
	}

	public static Point getBestUnvisited(Heuristic heuristic, int xV, int yV,Pathfinder pathfinder) {
		ArrayList<Point> points = getUnvisitedList(xV, yV, pathfinder.graph,
				pathfinder.validMovements);

		Point best = null;
		for (Point p : points)
			if (!isVisited((int) p.x, (int) p.y) && !pathfinder.isNodeBlocked(p)) {
				if (best == null) {
					best = p;
					// System.out.println("gotbest");
				} else if (heuristic.compare(p, best) == 0) {
					best = p;
					// System.out.println("better");
				}
			}/*
			 * else System.out.println("-"+(int) p.x+ " "+ (int) p.y);
			 */

		if (best == null)
			return best;

		// System.out.println("Best "+best);
		visit((int) best.x, (int) best.y);
		return best;
	}
}
