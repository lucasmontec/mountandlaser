package com.carvalab.mountandlaser.framework;

import java.util.ArrayList;

import com.carvalab.mountandlaser.game.Wall;

public class Map {

	private int width, height;

	private ArrayList<Wall> walls;

	public Map(int w, int h) {
		width = w;
		height = h;

		// Add the limit walls
		walls = new ArrayList<>();
		// Left wall
		walls.add(new Wall(-50, -50, 50, height + 50));
		// Top wall
		walls.add(new Wall(-50, -50, width + 50, 50));
		// Bottom wall
		walls.add(new Wall(-50, height, width + 50, 50));
		// Right wall
		walls.add(new Wall(width, -50, 50, height + 50));
	}

	public void addWall(Wall w) {
		if (!walls.contains(w))
			walls.add(w);
	}

	public ArrayList<Wall> getWalls() {
		return walls;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
