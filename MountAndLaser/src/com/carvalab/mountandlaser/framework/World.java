package com.carvalab.mountandlaser.framework;

import java.awt.Graphics2D;
import java.util.Collection;
import java.util.Stack;

import com.carvalab.mountandlaser.collision.GridBroadphase;
import com.carvalab.mountandlaser.collision.ICollisionSolver;
import com.carvalab.mountandlaser.collision.INarrowPhase;
import com.carvalab.mountandlaser.collision.ShapeNarrowphase;
import com.carvalab.mountandlaser.collision.ShapeSimpleBounceSolver;
import com.carvalab.mountandlaser.core.GameObject;
import com.carvalab.mountandlaser.framework.camera.Camera;
import com.carvalab.mountandlaser.unit.Unit;
import com.carvalab.mountandlaser.unit.UnitManager;

public class World {
	
	private static UnitManager unitman;
	private static Stack<Unit> addStack;
	private static GridBroadphase broadphase;
	private static INarrowPhase narrowphase;
	private static ICollisionSolver solver;
	private static final int gridDivision = 4;
	
	public static Collection<? extends GameObject> getUnits(){
		return unitman.objects();
	}
	
	public static void startWorld(int width, int height) {
		startWorld(width, height, new ShapeNarrowphase(), new ShapeSimpleBounceSolver());
	}
	
	public static void startWorld(int width, int height, INarrowPhase enarrowphase, ICollisionSolver esolver) {
		unitman = new UnitManager();
		broadphase = new GridBroadphase(width, height, gridDivision);
		narrowphase = enarrowphase;
		solver = esolver;
	}
	
	public static void startWorld(Map m, INarrowPhase enarrowphase, ICollisionSolver esolver) {
		startWorld(m.getWidth(),m.getHeight(),enarrowphase,esolver);
		setMap(m);
	}
	
	public static void startWorld(Map m) {
		startWorld(m.getWidth(), m.getHeight(), new ShapeNarrowphase(), new ShapeSimpleBounceSolver());
		setMap(m);
	}
	
	public static UnitManager unitManager() {
		return unitman;
	}
	
	public static void setMap(Map m) {
		if(broadphase == null)
			throw new RuntimeException("Broadphase cannot be null!");
		if(m == null)
			throw new RuntimeException("Map cannot be null!");
		m.getWalls().forEach(w -> {broadphase.addObjectToMaster(w);});
	}
	
	public static void addUnit(Unit u) {
		if(addStack == null)
			addStack = new Stack<>();
		addStack.push(u);
	}

	public static void update(float delta) {
		if(addStack != null && !addStack.isEmpty())
			while(!addStack.isEmpty())
				unitman.addObject(addStack.pop());
		
		unitman.update(delta);
		
		// Check collision
		broadphase.buildAndApply(unitman.objects(), narrowphase, solver);
	}

	public static void render(Graphics2D g, Camera c) {
		unitman.render(g, c);
		//Render master
		broadphase.getMaster().forEach( go -> { go.render(g);} );
	}
	
	public static void render(Graphics2D g) {
		render(g, null);
	}
}
