package com.carvalab.mountandlaser.framework.geom;

import java.awt.Graphics2D;

import com.carvalab.mountandlaser.collision.CollisionGroup;


public abstract class Shape {

	private CollisionGroup collisionGroup;
	
	public abstract boolean intersects(Shape s);
	
	public abstract boolean contains(Point p);
	
	public abstract Point center();
	
	public abstract void renderDebug(Graphics2D g);
	
	public abstract float getWidth();
	
	public abstract float getHeight();
	
	public abstract void setCenter(Point p);
	
	public Shape() {
		ShapeDebug.registerShape(this);
		collisionGroup = CollisionGroup.WORLD;
	}

	public CollisionGroup getCollisionGroup() {
		return collisionGroup;
	}

	public Shape setCollisionGroup(CollisionGroup collisionGroup) {
		this.collisionGroup = collisionGroup;
		return this;
	}
}
