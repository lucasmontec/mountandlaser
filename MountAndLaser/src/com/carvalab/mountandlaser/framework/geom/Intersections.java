package com.carvalab.mountandlaser.framework.geom;

public class Intersections {

	public static boolean circunferenceToCircunference(Circunference a,
			Circunference b) {
		// Center distances is less then the sum of the radius
		float centerDistances = a.center().distance(b.center);
		float radiusSum = a.radius + b.radius;
		return centerDistances < radiusSum;
	}

	public static boolean rectangleToRectangle(Rectangle a, Rectangle b) {
		if (a.topLeft.x + a.width < b.topLeft.x
				|| a.topLeft.y + a.height < b.topLeft.y
				|| a.topLeft.x > b.topLeft.x + b.width
				|| a.topLeft.y > b.topLeft.y + b.height)
			return false;
		return true;
	}

	public static boolean circunferenceToRectangle(Circunference circ,
			Rectangle rect) {
		// Border check
		// Y collision
		if (circ.center.x > rect.topLeft.x
				&& circ.center.x < rect.topLeft.x + rect.width) {
			boolean YintersectionTop = (circ.center.y + circ.radius > rect.topLeft.y)
					&& (circ.center.y < rect.topLeft.y + rect.height);
			boolean YintersectionBottom = (circ.center.y - circ.radius < rect.topLeft.y
					+ rect.height)
					&& (circ.center.y > rect.topLeft.y);
			return YintersectionTop || YintersectionBottom;
		} else {
			// Corner check
			if (circ.center.x < rect.topLeft.x) { // Corners to the left
				if (circ.center.y < rect.topLeft.y)// Top left corner
					return circ.center.distance(rect.topLeft) < circ.radius;
				else if (circ.center.y > rect.topLeft.y + rect.height)// Bottom
																		// left
																		// corner
					return circ.center.distance(rect.topLeft.x, rect.topLeft.y
							+ rect.height) < circ.radius;
			} else {// Corners to the right
				if (circ.center.y < rect.topLeft.y)// Top right corner
					return circ.center.distance(rect.topLeft.x + rect.width,
							rect.topLeft.y) < circ.radius;
				else if (circ.center.y > rect.topLeft.y + rect.height)// Bottom
																		// right
																		// corner
					return circ.center.distance(rect.bottomRight()) < circ.radius;
			}
		}
		// X collision
		if (circ.center.y > rect.topLeft.y
				&& circ.center.y < rect.topLeft.y + rect.height) {
			boolean XintersectionLeft = (circ.center.x + circ.radius > rect.topLeft.x)
					&& (circ.center.x < rect.topLeft.x + rect.width);
			boolean XintersectionRight = (circ.center.x - circ.radius < rect.topLeft.x
					+ rect.width)
					&& (circ.center.x > rect.topLeft.x);
			return XintersectionLeft || XintersectionRight;
		}

		return false;
	}

	public static Point minimumTranslationCircToRect(Circunference circ,
			Rectangle rect) {
		return minimumTranslationRectToRect(rect, circ.boundingRect());
	}

	public static Point minimumTranslationCircToCirc(Circunference a,
			Circunference b) {
		Point mt = b.center.copy();

		float centerDistances = a.center().distance(b.center);
		float radiusSum = a.radius + b.radius;
		float len = Math.abs(radiusSum - centerDistances);

		// Calculate movement in x
		mt.sub(a.center).asVectorNormalize().mul(len);

		return mt;
	}

	public static Point minimumTranslationRectToRect(Rectangle a, Rectangle b) {
		Point mt = new Point();

		float left = b.topLeft.x - a.bottomRight().x;
		float right = b.bottomRight().x - a.topLeft.x;
		float top = b.topLeft.y - a.bottomRight().y;
		float bottom = b.bottomRight().y - a.topLeft.y;

		if (left > 0 || right < 0)
			throw new RuntimeException("No intersection");
		if (top > 0 || bottom < 0)
			throw new RuntimeException("No intersection");

		if (Math.abs(left) < right)
			mt.x = left;
		else
			mt.x = right;

		if (Math.abs(top) < bottom)
			mt.y = top;
		else
			mt.y = bottom;

		// remove the biggest value.
		if (Math.abs(mt.x) < Math.abs(mt.y))
			mt.y = 0;
		else
			mt.x = 0;

		return mt;
	}
}
