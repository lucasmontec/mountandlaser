package com.carvalab.mountandlaser.framework.geom;

public class Intersections {

	public static boolean circunferenceToCircunference(Circunference a, Circunference b) {
		// Center distances is less then the sum of the radius
		float centerDistances = a.center().distance(b.center);
		float radiusSum = a.radius + b.radius;
		return centerDistances < radiusSum;
	}

	public static boolean rectangleToRectangle(Rectangle a, Rectangle b) {
		if (a.topLeft.x + a.width < b.topLeft.x || a.topLeft.y + a.height < b.topLeft.y
				|| a.topLeft.x > b.topLeft.x + b.width || a.topLeft.y > b.topLeft.y + b.height) {
			return false;
		}
		return true;
	}

	// http://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection
	public static boolean circunferenceToRectangle(Circunference circle, Rectangle rect) {
		float circleDistancex = Math.abs(circle.center.x - rect.center().x);
		float circleDistancey = Math.abs(circle.center.y - rect.center().y);

		if (circleDistancex > (rect.width / 2f + circle.radius)) {
			return false;
		}
		if (circleDistancey > (rect.height / 2f + circle.radius)) {
			return false;
		}

		if (circleDistancex <= (rect.width / 2f)) {
			return true;
		}
		if (circleDistancey <= (rect.height / 2f)) {
			return true;
		}

		float cornerDistance_sq = (float) (Math.pow((circleDistancex - rect.width / 2f), 2f)
				+ Math.pow((circleDistancey - rect.height / 2f), 2f));

		return (cornerDistance_sq <= Math.pow(circle.radius, 2f));
	}

	public static Point minimumTranslationCircToRect(Circunference circ, Rectangle rect) {
		return minimumTranslationRectToRect(rect, circ.boundingRect());
	}

	public static Point minimumTranslationCircToCirc(Circunference a, Circunference b) {
		Point mt = b.center.copy();

		float centerDistances = a.center().distance(b.center);
		float radiusSum = a.radius + b.radius;
		float len = Math.abs(radiusSum - centerDistances);

		// Calculate movement in x
		mt.sub(a.center).asVectorNormalize().mul(len);

		return mt;
	}

	public static Point minimumTranslationRectToRect(Rectangle a, Rectangle b) {
		Point mt = new Point();

		float left = b.topLeft.x - a.bottomRight().x;
		float right = b.bottomRight().x - a.topLeft.x;
		float top = b.topLeft.y - a.bottomRight().y;
		float bottom = b.bottomRight().y - a.topLeft.y;

		if (left > 0 || right < 0) {
			throw new RuntimeException("No intersection");
		}
		if (top > 0 || bottom < 0) {
			throw new RuntimeException("No intersection");
		}

		if (Math.abs(left) < right) {
			mt.x = left;
		} else {
			mt.x = right;
		}

		if (Math.abs(top) < bottom) {
			mt.y = top;
		} else {
			mt.y = bottom;
		}

		// remove the biggest value.
		if (Math.abs(mt.x) < Math.abs(mt.y)) {
			mt.y = 0;
		} else {
			mt.x = 0;
		}

		return mt;
	}
}
