package com.carvalab.mountandlaser.framework.geom;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

public class ShapeDebug {

	private static ArrayList<Shape> shapes;
	private static boolean started = false;
	
	public static void start() {
		started = true;
	}

	private static ArrayList<Shape> Instance() {
		if(!started)
			return null;
		if (shapes == null)
			shapes = new ArrayList<>();
		return shapes;
	}

	public static synchronized void registerShape(Shape s) {
		if (started) {
			if(s instanceof Point)
				return;
			Instance().add(s);
			//System.out.println("Shape registered.");
		}
	}

	public static synchronized void clear() {
		Instance().clear();
	}
	
	public static synchronized void render(Graphics2D g) {
		if (started) {
			g.setColor(Color.green);
			for (Shape s : Instance())
				s.renderDebug(g);
		}
	}
}
