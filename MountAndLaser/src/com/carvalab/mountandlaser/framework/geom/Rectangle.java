package com.carvalab.mountandlaser.framework.geom;

import java.awt.Graphics2D;

public class Rectangle extends Shape {

	public Point topLeft;
	public float width, height;

	public Rectangle() {
		super();
		width = height = 10;
		topLeft = new Point();
	}

	public Rectangle(Point topLeft, float width, float height) {
		super();
		this.topLeft = new Point(topLeft);
		this.width = width;
		this.height = height;
	}

	public Rectangle(int width, int height) {
		this.topLeft = new Point();
		this.width = width;
		this.height = height;
	}

	@Override
	public boolean intersects(Shape s) {
		if (s instanceof Circunference) {
			return Intersections.circunferenceToRectangle((Circunference)s, this);
		} else if (s instanceof Rectangle) {
			//Check both this with that and that with this in case one is bigger than the other
			return Intersections.rectangleToRectangle(this, (Rectangle)s);
		}else if(s instanceof Point) {
			return contains((Point)s);
		}

		return false;
	}

	public Point bottomRight() {
		return new Point(topLeft).add(width, height);
	}

	@Override
	public boolean contains(Point p) {
		return p.x > topLeft.x && p.x < topLeft.x + width && p.y > topLeft.y
				&& p.y < topLeft.y + height;
	}

	@Override
	public Point center() {
		return new Point((topLeft.x + width / 2), (topLeft.y + height / 2));
	}

	@Override
	public void renderDebug(Graphics2D g) {
		g.drawRect((int)topLeft.x, (int)topLeft.y, (int)width, (int)height);
	}

	@Override
	public float getWidth() {
		return width;
	}

	@Override
	public float getHeight() {
		return height;
	}

	@Override
	public void setCenter(Point p) {
		topLeft.x = p.x-width/2;
		topLeft.y = p.y-height/2;
	}

}
