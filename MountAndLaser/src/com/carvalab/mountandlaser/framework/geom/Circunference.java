package com.carvalab.mountandlaser.framework.geom;

import java.awt.Graphics2D;

public class Circunference extends Shape {

	public Point center;
	public float radius;

	public Circunference() {
		super();
		center = new Point();
	}

	public Circunference(Point center, float radius) {
		super();
		this.center = new Point(center);
		this.radius = radius;
	}

	@Override
	public boolean intersects(Shape s) {
		if (s instanceof Circunference) {
			return Intersections.circunferenceToCircunference(this, (Circunference)s);
		} else if (s instanceof Rectangle) {
			return Intersections.circunferenceToRectangle(this, (Rectangle)s);
		}else if(s instanceof Point) {
			return contains((Point)s);
		}
		return false;
	}

	public Rectangle boundingRect() {
		return new Rectangle(center.subR(radius, radius), radius*2, radius*2);
	}
	
	@Override
	public boolean contains(Point p) {
		return p.distance(center) < radius;
	}

	@Override
	public Point center() {
		return center;
	}

	@Override
	public void renderDebug(Graphics2D g) {
		g.drawOval((int)(center.x-radius), (int)(center.y-radius), (int)radius*2, (int)radius*2);
		g.fillOval((int)center.x, (int)center.y, 2, 2);
	}

	@Override
	public float getWidth() {
		return radius;
	}

	@Override
	public float getHeight() {
		return radius;
	}

	@Override
	public void setCenter(Point p) {
		center.set(p);
	}

}
