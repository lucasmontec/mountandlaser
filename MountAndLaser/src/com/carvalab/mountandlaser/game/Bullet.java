package com.carvalab.mountandlaser.game;

import com.carvalab.mountandlaser.collision.CollisionGroup;
import com.carvalab.mountandlaser.framework.Assets;
import com.carvalab.mountandlaser.framework.geom.Circunference;
import com.carvalab.mountandlaser.unit.Projectile;

public class Bullet extends Projectile {

	public float fireAngle;
	public float speed = 0.05f;

	public Bullet() {
	}

	public Bullet(int power, float fireAngle, float x, float y) {
		this.power = power;
		this.fireAngle = fireAngle;
		pos.x = x;
		pos.y = y;
		setVelocity((float)Math.cos(fireAngle), (float)Math.sin(fireAngle));
		getVelocity().asVectorNormalize();
	}
	
	{
		setAnimation(Assets.bullet);
		setBody(new Circunference(pos.addR(getWidth()/2, getHeight()/2), 7));
		getBody().setCollisionGroup(CollisionGroup.PROJECTILE);
	}

	@Override
	public void logic(float dt) {
		getVelocity().add(getVelocity().copy().asVectorNormalize().mul(speed));
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public void setFireAngle(float fireAngle) {
		this.fireAngle = fireAngle;
	}

}
