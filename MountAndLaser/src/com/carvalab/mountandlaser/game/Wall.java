package com.carvalab.mountandlaser.game;

import java.awt.Color;
import java.awt.Graphics2D;

import com.carvalab.mountandlaser.collision.CollisionGroup;
import com.carvalab.mountandlaser.core.GameObject;
import com.carvalab.mountandlaser.framework.geom.Point;
import com.carvalab.mountandlaser.framework.geom.Rectangle;

public class Wall extends GameObject{

	public Wall(float x, float y, float w, float h) {
		setMass(0f);
		setBody(new Rectangle(new Point(x,y), w, h).setCollisionGroup(CollisionGroup.WORLD));
	}
	
	@Override
	public void update(float dt) {
	}

	@Override
	public void render(Graphics2D g) {
		g.setColor(Color.red);
		getBody().renderDebug(g);
	}

	@Override
	public void collided(GameObject hit) {
	}

}
