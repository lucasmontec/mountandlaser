package com.carvalab.mountandlaser.game;

import com.carvalab.mountandlaser.core.Animation;
import com.carvalab.mountandlaser.core.GameObject;
import com.carvalab.mountandlaser.framework.Assets;
import com.carvalab.mountandlaser.unit.LiveUnit;

public class Enemy extends LiveUnit{

	private Animation idle;
	private Animation dead;
	
	public Enemy() {
		idle = Assets.getEnemyIdle();
		dead = Assets.animationState("demon", "dead", 0.1f);
		setAnimation(idle);
		setMaxHealth(30);
	}
	
	@Override
	public void logic(float dt) {
		if(!isAlive() && !getAnimation().equals(dead)) {
			dead.reset();
			setAnimation(dead);
		}
		
		if(dead.isComplete())
			setToRemove();
	}

	@Override
	public void collided(GameObject hit) {
	}

}
