package com.carvalab.mountandlaser.game;

import com.carvalab.mountandlaser.core.Animation;
import com.carvalab.mountandlaser.core.GameObject;
import com.carvalab.mountandlaser.framework.World;
import com.carvalab.mountandlaser.unit.LiveUnit;

public class Hero extends LiveUnit {

	/**
	 * Main hero level
	 */
	private int level;
	private int experience;
	private int mousex, mousey;
	private float speed;

	private Animation idle;
	private Animation fire;
	private Animation dead;

	public Hero(Animation idle, Animation fire, Animation dead) {
		this.idle = idle;
		this.fire = fire;
		this.dead = dead;
		speed = 100f;

		setAnimation(idle);
	}

	public void moveForward(float dt) {
		pos.x += dt * speed * Math.cos(getAngle());
		pos.y += dt * speed * Math.sin(getAngle());
	}

	public void moveRight(float dt) {
		pos.x += dt * speed * Math.cos(getAngle() + Math.PI / 2);
		pos.y += dt * speed * Math.sin(getAngle() + Math.PI / 2);
	}

	public void setMouse(float x, float y) {
		mousex = (int) x;
		mousey = (int) y;
	}

	public void addExperience(int amount) {
		experience += amount;
		level = (int) (Math.sqrt(experience) / 10);
	}

	public int getLevel() {
		return level;
	}

	public long getExperience() {
		return experience;
	}

	public float getLevelProgress() {
		long currentLevelExp = level * level * 10;
		int nextLevel = level + 1;
		long nextLevelExp = nextLevel * nextLevel * 10;

		return (experience - currentLevelExp) / (nextLevelExp - experience);
	}

	public long getExpLeftToNextLevel() {
		long currentLevelExp = level * level * 10;
		int nextLevel = level + 1;
		long nextLevelExp = nextLevel * nextLevel * 10;
		long expDifference = nextLevelExp - currentLevelExp;
		// Exp difference between levels - how much exp the player got in this
		// level
		return expDifference - (experience - currentLevelExp);
	}

	public void fire() {
		World.addUnit(new Bullet(15, this.getAngle(), 
				(float) (getBody().center().x-10 + Math.cos(getAngle())*70),
				(float) (getBody().center().y-10 + Math.sin(getAngle())*70))
				);
	}

	@Override
	public void logic(float dt) {
		// Mouse look
		setAngle((float) (Math.PI + Math.atan2((pos.y + getFrameHeight() / 2)
				- mousey, (pos.x + getFrameWidth() / 2) - mousex)));
	}

	@Override
	public void collided(GameObject hit) {
	}

}
