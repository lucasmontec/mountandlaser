package com.carvalab.mountandlaser.unit;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import com.carvalab.mountandlaser.core.Animation;
import com.carvalab.mountandlaser.core.GameObject;
import com.carvalab.mountandlaser.framework.geom.Point;
import com.carvalab.mountandlaser.framework.geom.Rectangle;
import com.carvalab.mountandlaser.framework.geom.Shape;

public abstract class Unit extends GameObject {

	private Animation currentAnimation;
	private float angle = 0f;
	private AffineTransform transform;
	private Point velocity;
	
	{
		velocity = new Point();
	}

	public float getAngle() {
		return angle;
	}

	public void setAngle(float angle) {
		this.angle = angle;
	}

	public Unit() {
		transform = new AffineTransform();
	}

	public void setAnimation(Animation anim) {
		currentAnimation = anim;
	}

	@Override
	public void render(Graphics2D g) {
		if (currentAnimation != null) {
			transform.setToTranslation(0, 0);
			transform.translate(pos.x, pos.y);
			transform.rotate(angle, getFrameWidth() / 2.0f,
					getFrameHeight() / 2.0f);
			g.drawImage(currentAnimation.getCurrentFrame(), transform, null);
		}
	}

	@Override
	public void update(float dt) {
		if (currentAnimation != null)
			currentAnimation.update(dt);

		// Bind the shape to the position
		if (body != null)
			body.setCenter(pos.addR(getFrameWidth() / 2, getFrameHeight() / 2));

		// Move velocity
		if(isMovable())
			pos.add(velocity.mulR(dt));

		logic(dt);
	}

	public abstract void logic(float dt);

	public int getFrameWidth() {
		return currentAnimation.getWidth();
	}

	public int getFrameHeight() {
		return currentAnimation.getHeight();
	}

	public float getWidth() {
		if (body != null)
			return body.getWidth();
		else
			return getFrameWidth();
	}

	public float getHeight() {
		if (body != null)
			return body.getHeight();
		else
			return getFrameHeight();
	}

	private void makeDefaultBody() {
		body = new Rectangle(pos, getFrameWidth(), getFrameHeight());
	}

	@Override
	public Shape getBody() {
		if (body == null)
			makeDefaultBody();
		return body;
	}

	public Point getVelocity() {
		return velocity;
	}

	public void setVelocity(Point velocity) {
		this.velocity = velocity;
	}

	public void setVelocity(float x, float y) {
		this.velocity.set(x, y);
	}

	public void accelerate(Point velocity) {
		this.velocity.add(velocity);
	}

	public void accelerate(float x, float y) {
		this.velocity.add(x, y);
	}
	
	public Animation getAnimation() {
		return currentAnimation;
	}
}
