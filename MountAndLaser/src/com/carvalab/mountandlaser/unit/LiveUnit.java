package com.carvalab.mountandlaser.unit;

public abstract class LiveUnit extends Unit {

	protected int health = 100;
	protected int maxHealth = 100;
	protected boolean invulnerable = false;
	
	public boolean isAlive() {
		return health > 0;
	}
	
	public void setMaxHealth(int val) {
		if(val > 0) {
			maxHealth = val;
			if(health > maxHealth)
				health = maxHealth;
		}
	}
	
	public int getHealth() {
		return health;
	}

	/**
	 * Can't damage if invulnerable.
	 * Can only cause damage.
	 * @param amount Subtracts amount from health
	 */
	public void damage(int amount) {
		if(invulnerable) return;
		if(amount < 0) return;
		health -= amount;
		if (health < 0)
			health = 0;
	}
	
	/**
	 * Can only heal.
	 * Health += amount.
	 * Amount must be positive.
	 * @param amount
	 */
	public void heal(int amount) {
		if(amount < 0) return;
		health += amount;
		if(health > maxHealth)
			health = maxHealth;
	}
	
	/**
	 * Ignores invulnerability.
	 * @param val If the val is between 0 and MaxHealth, set the health to it.
	 */
	public void setHealth(int val) {
		if(val > 0 && val < maxHealth)
			health = val;
	}
	
	/**
	 * Sets the health with no limitations
	 * @param val
	 */
	public void setHealthUnbound(int val) {
		health = val;
	}
}
