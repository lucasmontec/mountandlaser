package com.carvalab.mountandlaser.unit;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.carvalab.mountandlaser.framework.camera.Camera;

public class UnitManager {

	private HashMap<String, Unit> objects;
	private ArrayList<String> removeList;


	public UnitManager() {
		objects = new HashMap<>();
		removeList = new ArrayList<>();
	}

	public void addObject(Unit obj) {
		objects.put(obj.getID(), obj);
	}

	public synchronized void update(float dt) {
		// Tag to remove objects that are inactive
		// Update others
		objects.forEach((s, o) -> {
			if (!o.shouldRemove())
				o.update(dt);
			else
				removeList.add(s);
		});

		// Remove objects tagged for removal before collision
		if (removeList.size() > 0) {
			removeList.forEach(s -> objects.remove(s));
			removeList.clear();
		}
	}

	public synchronized void render(Graphics2D g, Camera c) {
		objects.forEach((s, o) -> {
			if(c == null || (c != null && c.inView(o)))
				o.render(g);
		});
	}

	public Collection<Unit> objects() {
		return objects.values();
	}

}
