package com.carvalab.mountandlaser.unit;

import com.carvalab.mountandlaser.core.GameObject;

public abstract class Projectile extends Unit{

	public int power = 10;
	
	public Projectile() {}
	
	public Projectile(int power) { this.power = power; }
	
	/**
	 * Power < 0 will heal the target.
	 * Power > 0 will damage the target.
	 * @param v
	 */
	public void setPower(int v) {
		power = v;
	}

	@Override
	public void collided(GameObject hit) {
		if(hit instanceof LiveUnit)
			if(power > 0) {
				((LiveUnit) hit).damage(power);
			}else
				((LiveUnit) hit).heal(-power);
		
		//System.out.println("collision");
		//Die
		setToRemove();
	}

}
