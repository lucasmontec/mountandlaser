package com.carvalab.mountandlaser.core;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;

public class Animation {

	private ArrayList<BufferedImage> frames;
	private BufferedImage currentFrame;
	private float time;
	private float duration;
	private float frameDuration;
	private boolean looping;
	private boolean pingpong;
	private boolean complete;
	private int width, height;

	public Animation(float frameDuration) {
		frames = new ArrayList<>();
		this.frameDuration = frameDuration;
		looping = false;
	}

	public Animation(float frameDuration, BufferedImage... frames) {
		this.frames = new ArrayList<>();
		this.frameDuration = frameDuration;

		for (BufferedImage frame : frames) {
			addFrame(frame);
		}
	}

	public Animation setLooping(boolean set) {
		looping = set;
		return this;
	}

	public void addFrame(BufferedImage frame) {
		frames.add(frame);
		currentFrame = frame;
		duration += frameDuration;

		width = frame.getWidth();
		height = frame.getHeight();
	}

	public void addFrames(BufferedImage... frames) {
		for (BufferedImage frame : frames) {
			addFrame(frame);
		}
	}
	
	public void reset() {
		time = 0f;
		complete = false;
	}
	
	public void update(float dt) {
		if (time < duration)
			time += dt;
		else if (looping) {
			time = 0f;
			if(pingpong)
				Collections.reverse(frames);
		}else
			complete = true;
		
		int index = Math.round((time / duration)*(frames.size()-1));
		if (frames.size() > 1 && index < frames.size())
			currentFrame = frames.get(index);
	}

	public BufferedImage getCurrentFrame() {
		return currentFrame;
	}

	public float getDuration() {
		return duration;
	}

	public boolean isLooping() {
		return looping;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public boolean isComplete() {
		return !looping && complete;
	}
	
	public Animation setPingPong(boolean val) {
		pingpong = val;
		return this;
	}
}
