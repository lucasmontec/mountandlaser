package com.carvalab.mountandlaser.core.test;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import com.carvalab.mountandlaser.collision.CollisionGroup;
import com.carvalab.mountandlaser.core.GameObject;
import com.carvalab.mountandlaser.framework.Assets;
import com.carvalab.mountandlaser.framework.Map;
import com.carvalab.mountandlaser.framework.Screen;
import com.carvalab.mountandlaser.framework.World;
import com.carvalab.mountandlaser.framework.ai.DirectProximityHeuristic;
import com.carvalab.mountandlaser.framework.ai.Heuristic;
import com.carvalab.mountandlaser.framework.ai.Pathfinder;
import com.carvalab.mountandlaser.framework.geom.Circunference;
import com.carvalab.mountandlaser.framework.geom.Point;
import com.carvalab.mountandlaser.framework.geom.ShapeDebug;
import com.carvalab.mountandlaser.game.Enemy;
import com.carvalab.mountandlaser.game.Hero;
import com.carvalab.mountandlaser.game.Wall;
import com.carvalab.mountandlaser.unit.Unit;

public class PathfindTest extends Screen {

	Hero h;
	Enemy enemy;
	Pathfinder pathfinder;
	ArrayList<Point> path;
	Heuristic heuristic;

	public PathfindTest() {
	}

	@Override
	public synchronized void render(Graphics2D g) {
		// Render objects
		World.render(g);
		ShapeDebug.render(g);

		pathfinder.renderDebug(g, path, h.getPos(), enemy.getPos());
	}

	@Override
	public synchronized void update(float delta) {
		h.setMouse(MOUSE.x, MOUSE.y);

		if (isKeyPressed(KeyEvent.VK_D))
			h.moveRight(delta);

		if (isKeyPressed(KeyEvent.VK_A))
			h.moveRight(-delta);

		if (isKeyPressed(KeyEvent.VK_W))
			h.moveForward(delta);

		if (isKeyPressed(KeyEvent.VK_S))
			h.moveForward(-delta);

		// update objects
		World.update(delta);

		// Get a path
		//if (path == null)
		pathfinder.updateDynamicBlockingNodes(World.getUnits());
		((DirectProximityHeuristic)heuristic).update(pathfinder, h.getPos());
			path = pathfinder.getHeuristicDFSPath(heuristic, enemy.getPos(),
					h.getPos());
	}

	@Override
	public void enter() {
		Map m = new Map(800, 600);
		m.addWall(new Wall(290, 180, 40, 200));
		World.startWorld(m);
		Assets.loadGameAssets();
		System.out.println("entered game");
		pathfinder = new Pathfinder(m, 10);

		h = new Hero(Assets.animationState("hero", "idle", 0.1f).setLooping(
				true), Assets.animationState("hero", "fire", 0.1f).setLooping(
				true), Assets.animationState("hero", "dead", 1f));

		enemy = new Enemy();
		enemy.setPos(new Point(500, 200));
		enemy.setMass(0);
		enemy.setMaxHealth(5000);

		h.setBody(new Circunference(h.getPos().addR(h.getFrameWidth() / 2,
				h.getFrameHeight() / 2), 50));
		h.setPos(new Point(120.37922f, 368.07062f));
		h.getBody().setCollisionGroup(CollisionGroup.PLAYER);

		heuristic = new DirectProximityHeuristic();

		Unit dynamicBlockingUnit = new Unit() {
			private boolean reverse;
			@Override
			public void collided(GameObject hit) {
			}
			
			@Override
			public void logic(float dt) {
				if(reverse && this.X() > 350)
					this.getPos().add(-10f*dt,0);
				else if(reverse && this.X() <= 350)
					reverse = !reverse;
				else if(!reverse && this.X() < 550)
					this.getPos().add(10f*dt,0);
				else if(!reverse && this.X() > 550)
					reverse = !reverse;
			}
		};
		dynamicBlockingUnit.setAnimation(Assets.animation("unit", 0.5f));
		dynamicBlockingUnit.setPos(new Point(350, 300));
		dynamicBlockingUnit.setDynamicObstacle(true);
		
		World.addUnit(dynamicBlockingUnit);
		World.addUnit(enemy);
		World.addUnit(h);
	}

	@Override
	public void leave() {
		Assets.unloadGameAssets();
	}

	@Override
	public void mousePressed() {
		if (MOUSE_LEFT)
			h.fire();
	}
}