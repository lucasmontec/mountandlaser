package com.carvalab.mountandlaser.core;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import com.carvalab.mountandlaser.collision.CollisionGroup;
import com.carvalab.mountandlaser.framework.Assets;
import com.carvalab.mountandlaser.framework.Map;
import com.carvalab.mountandlaser.framework.Screen;
import com.carvalab.mountandlaser.framework.World;
import com.carvalab.mountandlaser.framework.camera.Camera;
import com.carvalab.mountandlaser.framework.camera.TrackingCamera;
import com.carvalab.mountandlaser.framework.geom.Circunference;
import com.carvalab.mountandlaser.framework.geom.Point;
import com.carvalab.mountandlaser.framework.geom.ShapeDebug;
import com.carvalab.mountandlaser.game.Enemy;
import com.carvalab.mountandlaser.game.Hero;

public class Game extends Screen {

	Hero h;
	Camera c;
	
	public Game() {
	}

	@Override
	public synchronized void render(Graphics2D g) {
		c.film(g);
		// Render objects
		World.render(g,c);
		ShapeDebug.render(g);
		c.restore(g);
	}

	@Override
	public synchronized void update(float delta) {
		h.setMouse(MOUSE.x+c.topLeft().x, MOUSE.y+c.topLeft().y);

		if (isKeyPressed(KeyEvent.VK_D))
			h.moveRight(delta);

		if (isKeyPressed(KeyEvent.VK_A))
			h.moveRight(-delta);

		if (isKeyPressed(KeyEvent.VK_W))
			h.moveForward(delta);

		if (isKeyPressed(KeyEvent.VK_S))
			h.moveForward(-delta);

		// update objects
		World.update(delta);
	}

	@Override
	public void enter() {
		World.startWorld(new Map(1800,1600));
		Assets.loadGameAssets();
		//ShapeDebug.start();
		System.out.println("entered game");

		h = new Hero(Assets.animationState("hero", "idle", 0.1f).setLooping(
				true), Assets.animationState("hero", "fire", 0.1f).setLooping(
				true), Assets.animationState("hero", "dead", 1f));
		//h.setMass(0);

		Enemy test = new Enemy();
		test.setPos(new Point(500,200));
		test.setMass(0);

		h.setBody(new Circunference(h.pos.addR(h.getFrameWidth()/2, h.getFrameHeight()/2), 50));
		h.setPos(new Point(50, 200));
		h.getBody().setCollisionGroup(CollisionGroup.PLAYER);

		World.addUnit(test);
		World.addUnit(h);
		c = new TrackingCamera(WIDTH, HEIGHT, h);
	}

	@Override
	public void leave() {
		Assets.unloadGameAssets();
	}

	@Override
	public void mousePressed() {
		if (MOUSE_LEFT)
			h.fire();
	}
}
