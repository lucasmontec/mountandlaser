package com.carvalab.mountandlaser.core;

import javax.swing.JFrame;

import com.carvalab.mountandlaser.core.test.PathfindTest;
import com.carvalab.mountandlaser.framework.ScreenRenderer;

public class Main {

	private static ScreenRenderer renderer = new ScreenRenderer();
	
	public static void main(String... args) {
		JFrame gameFrame = new JFrame("Mount and Laser");
		gameFrame.setSize(800, 600);
		gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameFrame.setLocationRelativeTo(null);
		gameFrame.setResizable(false);
		gameFrame.setVisible(true);
		gameFrame.add(renderer);
		
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//renderer.addScreen("game", new Game());
		renderer.addScreen("game", new PathfindTest());
		renderer.start();
	}
	
}
