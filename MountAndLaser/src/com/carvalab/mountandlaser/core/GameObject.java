package com.carvalab.mountandlaser.core;

import java.awt.Graphics2D;

import com.carvalab.mountandlaser.framework.geom.Point;
import com.carvalab.mountandlaser.framework.geom.Shape;

public abstract class GameObject {

	protected Point pos;
	protected String ID;
	protected Shape body;
	protected float mass;
	protected boolean remove;
	protected boolean isDynamicObstacle;
	
	private static int UID = 0;

	{
		remove = false;
		pos = new Point();
		genID();
		mass = 1f;
	}
	
	public abstract void update(float dt);
	
	public abstract void render(Graphics2D g);
	
	public abstract void collided(GameObject hit);
	
	public void setToRemove() {
		remove = true;
	}
	
	public boolean shouldRemove() {
		return remove;
	}
	
	public String getID() {
		return ID;
	}
	
	public Point getPos() {
		return pos;
	}

	public float X() {
		return pos.x;
	}
	
	public float Y() {
		return pos.y;
	}
	
	public void setPos(Point pos) {
		this.pos = pos;
	}
	
	public void setBody(Shape s) {
		body = s;
	}
	
	public Shape getBody() {
		return body;
	}
	
	public float getMass() {
		return mass;
	}

	public void setMass(float mass) {
		this.mass = mass;
	}
	
	public boolean isMovable() {
		return mass > 0f;
	}
	
	private void genID() {
		ID = "OBJ_"+(UID++);
	}
	
	public float getWidth() {
		if (body != null)
			return body.getWidth();
		else
			return 0f;
	}

	public float getHeight() {
		if (body != null)
			return body.getHeight();
		else
			return 0f;
	}

	public boolean isDynamicObstacle() {
		return isDynamicObstacle;
	}

	public void setDynamicObstacle(boolean isDynamicObstacle) {
		this.isDynamicObstacle = isDynamicObstacle;
	}
}
